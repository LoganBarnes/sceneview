import { vec2, vec3, mat3, mat4 } from "gl-matrix";
import pointsVert from "ts-shader-loader!@/assets/shaders/points.vert";
import pointsFrag from "ts-shader-loader!@/assets/shaders/points.frag";
import {
  GLUtils,
  GLProgram,
  GLBuffer,
  GLVertexArray,
  Camera,
  Renderable,
  Scene
} from "ts-graphics";

class Points extends Renderable {
  private glUtils: GLUtils;
  private program: GLProgram;
  private vbo: GLBuffer;
  private vao: GLVertexArray;
  private modelMatrix: mat4;
  private normalsMatrix: mat3;
  private totalPoints: number;
  private viewport: vec2;

  constructor(glUtils: GLUtils) {
    super();
    this.glUtils = glUtils;
    this.modelMatrix = mat4.create();
    this.normalsMatrix = mat3.create();
    this.totalPoints = 1000000;
    this.viewport = vec2.fromValues(640, 480);

    const gl: WebGLRenderingContext = this.glUtils.getContext();

    this.program = this.glUtils.createProgram(
      {
        text: pointsVert,
        type: gl.VERTEX_SHADER
      },
      {
        text: pointsFrag,
        type: gl.FRAGMENT_SHADER
      }
    );

    const data: Float32Array = new Float32Array(this.totalPoints * 8);

    let point: vec3 = vec3.fromValues(0, 0, 0);
    let normal: vec3 = vec3.fromValues(0, 0, 0);
    const texCoords: vec2 = vec2.fromValues(0, 0);

    for (let i: number = 0; i < this.totalPoints; ++i) {
      const face: number = Math.floor(Math.random() * 6);

      normal.set([0, 0, 0]);
      texCoords[0] = Math.random();
      texCoords[1] = Math.random();

      switch (face) {
        case 0: // pos z
          normal[2] = 1;
          point.set([texCoords[0] - 0.5, texCoords[1] - 0.5, 0.5]);
          break;
        case 1: // pos x
          normal[0] = 1;
          point.set([0.5, texCoords[1] - 0.5, 0.5 - texCoords[0]]);
          break;
        case 2: // neg z
          normal[2] = -1;
          point.set([0.5 - texCoords[0], texCoords[1] - 0.5, -0.5]);
          break;
        case 3: // neg x
          normal[0] = -1;
          point.set([-0.5, texCoords[1] - 0.5, texCoords[0] - 0.5]);
          break;
        case 4: // pos y
          normal[1] = 1;
          point.set([texCoords[0] - 0.5, 0.5, 0.5 - texCoords[1]]);
          break;
        case 5: // neg y
          normal[1] = -1;
          point.set([texCoords[0] - 0.5, -0.5, texCoords[1] - 0.5]);
          break;
      }

      data[i * 8 + 0] = point[0];
      data[i * 8 + 1] = point[1];
      data[i * 8 + 2] = point[2];
      data[i * 8 + 3] = normal[0];
      data[i * 8 + 4] = normal[1];
      data[i * 8 + 5] = normal[2];
      data[i * 8 + 6] = texCoords[0];
      data[i * 8 + 7] = texCoords[1];
    }

    this.vbo = this.glUtils.createVbo(data);

    this.vao = this.glUtils.createVao(this.program, this.vbo, 8 * 4, [
      {
        name: "localPosition",
        size: 3,
        type: gl.FLOAT,
        offset: 0
      },
      {
        name: "localNormal",
        size: 3,
        type: gl.FLOAT,
        offset: 3 * 4
      },
      {
        name: "texCoords",
        size: 2,
        type: gl.FLOAT,
        offset: 6 * 4
      }
    ]);
  }

  onRender(camera: Camera, scene: Scene): void {
    const gl: WebGLRenderingContext = this.glUtils.getContext();

    this.program.use((): void => {
      this.program.setMatrixUniform(
        camera.screenFromWorldMatrix,
        "screenFromWorld"
      );

      this.program.setMatrixUniform(
        camera.screenFromViewMatrix,
        "screenFromCamera"
      );

      this.program.setMatrixUniform(this.modelMatrix, "worldFromLocal");
      this.program.setMatrixUniform(this.normalsMatrix, "worldFromLocalNormal");

      this.program.setFloatUniform(this.viewport, "viewport");

      this.program.setIntUniform(scene.displayMode, "displayMode");
      this.vao.render(gl.POINTS, 0, this.totalPoints);
    });
  }

  setPosition(trans: vec3): void {
    mat4.fromTranslation(this.modelMatrix, trans);
    mat3.normalFromMat4(this.normalsMatrix, this.modelMatrix);
  }

  onResize(w: number, h: number): void {
    vec2.set(this.viewport, w, h);
  }
}

export default Points;
