import { Scene, GLUtils } from "ts-graphics";
import Points from "./Points";

class PointsScene extends Scene {
  private points: Points;

  constructor(glUtils: GLUtils, w: number, h: number) {
    super();
    this.points = new Points(glUtils);

    this.addItemToRender(this.points);
  }

  onResize(w: number, h: number) {
    this.points.onResize(w, h);
  }
}

export default PointsScene;
