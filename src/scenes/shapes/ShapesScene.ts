import { Scene, GLUtils } from "ts-graphics";
import Quad from "./Quad";

class ShapesScene extends Scene {
  private shape: Quad;

  constructor(glUtils: GLUtils, w: number, h: number) {
    super();
    this.shape = new Quad(glUtils, w, h);

    this.addItemToRender(this.shape);
  }

  onResize(w: number, h: number) {
    // this.shape.onResize(w, h);
  }
}

export default ShapesScene;
