import { vec3, mat3, mat4 } from "gl-matrix";
import defaultVert from "ts-shader-loader!@/assets/shaders/default.vert";
import defaultFrag from "ts-shader-loader!@/assets/shaders/default.frag";
import {
  Cube,
  GLUtils,
  GLProgram,
  GLBuffer,
  GLVertexArray,
  Camera,
  Renderable,
  Scene
} from "ts-graphics";

class Quad extends Renderable {
  private glUtils: GLUtils;
  private program: GLProgram;
  private vbo: GLBuffer;
  private vao: GLVertexArray;
  private ibo: GLBuffer;
  private modelMatrix: mat4;
  private normalsMatrix: mat3;
  private numVertsToDraw: number;

  constructor(glUtils: GLUtils, viewWidth: number, viewHeight: number) {
    super();
    this.glUtils = glUtils;
    this.modelMatrix = mat4.create();
    this.normalsMatrix = mat3.create();

    const gl = this.glUtils.getContext();

    this.program = this.glUtils.createProgram(
      {
        text: defaultVert,
        type: gl.VERTEX_SHADER
      },
      {
        text: defaultFrag,
        type: gl.FRAGMENT_SHADER
      }
    );

    const cube: Cube = new Cube();

    const verts: Float32Array = cube.vertexData;
    const norms: Float32Array = cube.normalData;
    const tcrds: Float32Array = cube.texCoordData;

    const data: Float32Array = new Float32Array(
      verts.length + norms.length + tcrds.length
    );
    data.set(verts);
    data.set(norms, verts.length);
    data.set(tcrds, verts.length + norms.length);
    this.vbo = this.glUtils.createVbo(data);

    const indices: Uint16Array = cube.indexData;
    this.ibo = this.glUtils.createIbo(indices);

    this.numVertsToDraw = indices.length;

    this.vao = this.glUtils.createVao(this.program, this.vbo, 0, [
      {
        name: "localPosition",
        size: 3,
        type: gl.FLOAT,
        offset: 0
      },
      {
        name: "localNormal",
        size: 3,
        type: gl.FLOAT,
        offset: cube.getVertexDataSizeInBytes()
      },
      {
        name: "texCoords",
        size: 2,
        type: gl.FLOAT,
        offset:
          cube.getVertexDataSizeInBytes() + cube.getNormalDataSizeInBytes()
      }
    ]);
  }

  onRender(camera: Camera, scene: Scene): void {
    const gl: WebGLRenderingContext = this.glUtils.getContext();

    this.program.use((): void => {
      this.program.setMatrixUniform(
        camera.screenFromWorldMatrix,
        "screenFromWorld"
      );

      this.program.setMatrixUniform(this.modelMatrix, "worldFromLocal");
      this.program.setMatrixUniform(this.normalsMatrix, "worldFromLocalNormal");

      this.program.setIntUniform(scene.displayMode, "displayMode");
      this.vao.render(gl.TRIANGLES, 0, this.numVertsToDraw, this.ibo);
    });
  }

  setPosition(trans: vec3): void {
    mat4.fromTranslation(this.modelMatrix, trans);
    mat3.normalFromMat4(this.normalsMatrix, this.modelMatrix);
  }
}

export default Quad;
