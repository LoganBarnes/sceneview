#version 100
precision highp float;
precision highp int;

/*
 * Input
 */
attribute vec3 localPosition;
attribute vec3 localNormal;
attribute vec2 texCoords;
attribute vec3 color;

/*
 * Uniforms
 */
uniform mat4 screenFromWorld;
uniform mat4 screenFromCamera;
uniform mat4 worldFromLocal;
uniform mat3 worldFromLocalNormal;

uniform vec2 viewport;
// uniform float pointRadius;

/*
 * Output
 */
varying vec3 worldPosition;
varying vec3 worldNormal;
varying vec2 texc;
varying vec3 vertexColor;

/*
 * Pass all necessary variables to the fragment shader and
 * set the appropriate point size for the current view setup.
 */
void main() {
  worldPosition = vec3(worldFromLocal * vec4(localPosition, 1.0));
  worldNormal = worldFromLocalNormal * localNormal;
  texc = texCoords;
  vertexColor = color;

  float pointRadius = 0.002;

  gl_Position = screenFromWorld * worldFromLocal * vec4(worldPosition, 1.0);
  gl_PointSize = viewport.y * screenFromCamera[1][1] * pointRadius / gl_Position.w;
}
