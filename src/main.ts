// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import "vue-resize/dist/vue-resize.css";
import VueResize from "vue-resize";

Vue.config.productionTip = false;
Vue.use(VueResize);

/* eslint-disable no-new */
new Vue({
  el: "#app",
  router,
  components: { App },
  template: "<App/>"
});
