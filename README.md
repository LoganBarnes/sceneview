Sceneview
=========

[https://loganbarnes.gitlab.io/sceneview](https://loganbarnes.gitlab.io/sceneview/#/)

#### Controls

| Key                        | Function            |
| :------------------------: | :-----------------: |
| `right/left arrow`         | change scene        |
| `up/down arrow`            | change display mode |
| `left mouse drag`          | rotate camera       |
| `ctrl` + `left mouse drag` | pan camera          |
| `mouse scroll`             | zoom camera         |

#### Current Scenes

1) Cube Mesh
2) Cube Point Cloud

#### Render Modes

* Positions (x,y,z -> r,g,b)
* Normals (x,y,z -> r,g,b)
* Texture Coordinates (u,v,1 -> r,g,1)
* Texture (black for now)
* Uniform Color (white for now)
* White
